<?php
/**
 * k4 Redactor Removeformat plugin for Craft CMS 3.x
 *
 * Craft Redactor Remove Format is a Plugin for Craft CMS 3. It removes all formattings from the selected text in redactor.
 *
 * @link      http://www.kreisvier.ch
 * @copyright Copyright (c) 2017 Stefan Friedrich
 */

namespace k4\k4redactorremoveformat;


use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;
use craft\web\View;

use k4\k4redactorremoveformat\assetbundles\Remove;


use yii\base\Event;




/**
 * @author    Stefan Friedrich
 * @package   K4RedactorRemoveformat
 * @since     2.0.0
 */

class K4RedactorRemoveformat extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var static
     */
    public static $plugin;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        Event::on(
            Plugins::className(),
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                    // We were just installed
                }
            }
        );




    
        Event::on(View::class, View::EVENT_END_BODY, function(Event $event) {
            Craft::$app->getView()->registerAssetBundle(Remove::class);
        });
        

        
            Craft::info(Craft::t('k4redactorremoveformat', '{name} plugin loaded', ['name' => $this->name]), __METHOD__);

    }

    // Protected Methods
    // =========================================================================

}

