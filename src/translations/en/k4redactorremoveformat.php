<?php
/**
 * k4 Redactor Removeformat plugin for Craft CMS 3.x
 *
 * Craft Redactor Remove Format is a Plugin for Craft CMS 3. It removes all formattings from the selected text in redactor.
 *
 * @link      http://www.kreisvier.ch
 * @copyright Copyright (c) 2017 Stefan Friedrich
 */

/**
 * @author    Stefan Friedrich
 * @package   K4RedactorRemoveformat
 * @since     2.0.0
 */
return [
    'plugin loaded' => 'plugin loaded',
];
