<?php
/**
 * k4 Redactor Removeformat plugin for Craft CMS 3.x
 *
 * Craft Redactor Remove Format is a Plugin for Craft CMS 3. It removes all formattings from the selected text in redactor.
 *
 * @link      http://www.kreisvier.ch
 * @copyright Copyright (c) 2017 Stefan Friedrich
 */

/**
 * k4 Redactor Removeformat config.php
 *
 * Completely optional configuration settings for k4 Redactor Removeformat if you want to customize some
 * of its more esoteric behavior, or just want specific control over things.
 *
 * Don't edit this file, instead copy it to 'craft/config' as 'k4redactorremoveformat.php' and make
 * your changes there.
 *
 * Once copied to 'craft/config', this file will be multi-environment aware as well, so you can
 * have different settings groups for each environment, just as you do for 'general.php'
 */

return [

    // This controls blah blah blah
    "someSetting" => true,

];
