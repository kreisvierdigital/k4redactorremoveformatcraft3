<?php

namespace k4\k4redactorremoveformat\assetbundles;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    Stefan Friedrich
 * @package   k4redactorremoveformat
 * @since     2.0.0
 */
class Remove extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = __DIR__.'/dist';


        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'remove.js',
        ];

        parent::init();
    }
}
