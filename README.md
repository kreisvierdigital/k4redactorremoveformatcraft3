# k4 Redactor remove format plugin for Craft CMS 3.x

Craft Redactor remove format is a Plugin for Craft CMS 3. It removes all formattings from the selected text in redactor.

![Screenshot](resources/img/plugin-logo.png)

## Installation

To install k4 Redactor remove format, follow these steps:

1. Download & unzip the file and place the `k4redactorremoveformat` directory into your `craft/plugins` directory
2.  -OR- do a `git clone https://github.com/https://bitbucket.org/kreisvierdigital/redactorremoveformatcraft3/k4-redactor-removeformat.git` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3.  -OR- install with Composer via `composer require https://bitbucket.org/kreisvierdigital/redactorremoveformatcraft3/k4-redactor-removeformat`
4. Install plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `k4redactorremoveformat` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

k4 Redactor remove format works on Craft 3.x.


## Using k4 Redactor remove format

Add 'removeformat' to the plugins array in your Redactor plugins configuration: http://buildwithcraft.com/docs/rich-text-fields#redactor-configs):

```
{
buttons: ['html','formatting','bold','italic','link','image'],
plugins: ['fullscreen', 'removeformat'],
formattingTags: ['p', 'blockquote', 'pre', 'h1', 'h2', 'h3', 'h4'],
observeLinks: true,
toolbarFixedBox: true
}
```


Brought to you by [Stefan Friedrich](http://www.kreisvier.ch)
